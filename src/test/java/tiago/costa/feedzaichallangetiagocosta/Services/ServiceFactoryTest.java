/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Services;


import com.sun.javafx.scene.control.skin.VirtualFlow.ArrayLinkedList;
import java.util.List;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tiago.costa.feedzaichallangetiagocosta.Manager.DependecyManager;
import tiago.costa.feedzaichallangetiagocosta.Manager.IDependecyManager;

/**
 *
 * @author tiagocosta
 */
public class ServiceFactoryTest {
    
    public ServiceFactoryTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of Create method, of class ServiceFactory.
     */
    @Test
    public void testCreate() {
        System.out.println("Create");
        String className = "tiago.costa.feedzaichallangetiagocosta.Services.ServiceA";
        List<String> dependecies = new ArrayLinkedList<>();
        IDependecyManager manager = new DependecyManager();
        ServiceFactory instance = new ServiceFactory();
        IService expResult = new ServiceA(manager,dependecies);
        IService result = instance.Create(className, dependecies, manager);
        assertEquals(expResult.getClass(), result.getClass());
    }
    
}
