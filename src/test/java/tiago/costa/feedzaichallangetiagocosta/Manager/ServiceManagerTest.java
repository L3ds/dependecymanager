/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Manager;

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.net.URL;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;
import tiago.costa.feedzaichallangetiagocosta.Manager.Configurations.ConfigurationManager;
import tiago.costa.feedzaichallangetiagocosta.Services.ServiceFactory;

/**
 *
 * @author tiagocosta
 */
public class ServiceManagerTest {
    
    public ServiceManagerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

    /**
     * Test of StartAll method, of class ServiceManager.
     */
    @Test
    public void testStartAll() throws FileNotFoundException {
        System.out.println("StartAll");
        Gson gson = new Gson(); 
        URL url = this.getClass().getClassLoader().getResource("Config.JSON");
        ConfigurationManager configurationManager = new ConfigurationManager(url.getPath(), gson);
        DependecyManager dependecyManager = new DependecyManager();
        ServiceFactory serviceFactory = new ServiceFactory();
        ServiceManager instance = new ServiceManager(configurationManager, dependecyManager, serviceFactory);
        instance.StartAll();
    }
    
}
