/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Manager.Configurations;

import com.google.gson.Gson;
import java.io.FileNotFoundException;
import java.net.URL;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author tiagocosta
 */
public class ConfigurationManagerTest {
    
    
    
    public ConfigurationManagerTest() {
    }
    
    @BeforeClass
    public static void setUpClass() {
    }
    
    @AfterClass
    public static void tearDownClass() {
    }
    
    @Before
    public void setUp() {
    }
    
    @After
    public void tearDown() {
    }

  
    @Test(expected = FileNotFoundException.class)
    public void ConfigurationManager_Construtor_InvalidPath_ThrowsException() throws FileNotFoundException {
      
        Gson gson = new Gson();
        ConfigurationManager configurationManager = new ConfigurationManager("", gson);
        
    }
    
    /**
     *
     * @throws java.io.FileNotFoundException
     */
    @Test()
    public void ConfigurationManager_ValidConfiguration_GetServicesConfigurtion_ReturnsData() throws FileNotFoundException
    {
      
        Gson gson = new Gson();
        URL url = this.getClass().getClassLoader().getResource("Config.JSON");
        ConfigurationManager configurationManager = new ConfigurationManager(url.getPath(), gson);
        
        Assert.assertNotNull("Json Should have been seralized",configurationManager.GetServicesConfigurtion());
        Assert.assertTrue("Config.JSON had servies but configuration didn't load it",configurationManager.GetServicesConfigurtion().size()>0);
    }
}
