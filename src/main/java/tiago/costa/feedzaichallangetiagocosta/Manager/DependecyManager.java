/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Manager;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;
import tiago.costa.feedzaichallangetiagocosta.Services.IService;
import tiago.costa.feedzaichallangetiagocosta.Services.ServiceAbstract;

/**
 *
 * @author tiagocosta
 */
public class DependecyManager implements IDependecyManager {

    private final Object lock;
    private List<IService> RunningServices;
    private List<NotifyObject> NotifyObjects;

    public DependecyManager() {
        this.lock = new Object();
        this.RunningServices = new ArrayList<>();
        this.NotifyObjects = new ArrayList<>();
    }

    @Override
    public void WaitForDependeciesToStart(List<String> dependecyNames) {

        synchronized (lock) {
            Set<String> dependecyNamesSet = new HashSet<>(dependecyNames);

            boolean allDependeciesRunning = this.RunningServices
                    .stream()
                    .allMatch(p -> dependecyNamesSet.remove(p.GetName()))
                    && dependecyNamesSet.isEmpty();

            if (allDependeciesRunning) {
                return;

            }

            // Waiting for missing dependecies
            NotifyObject object = new NotifyObject(new ArrayList<>(dependecyNamesSet));

            object.WaitForDependecies();
        }

    }

    public void AddRunningService(IService service) {

        synchronized (lock) {
            this.RunningServices.add(service);

            this.NotifyObjects.stream().forEach(x -> x.CheckForDependencies(service));
        }

    }

}
