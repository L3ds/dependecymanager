/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Manager;

import java.util.HashSet;
import java.util.List;
import java.util.Set;
import tiago.costa.feedzaichallangetiagocosta.Services.IService;

/**
 *
 * @author tiagocosta
 */
public class NotifyObject {

    private final Object syncObject;
    private final List<String> Dependecies;

    public NotifyObject(List<String> dependencies) {
        this.Dependecies = dependencies;
        this.syncObject = new Object();
    }

    public void WaitForDependecies() {
        synchronized (this.syncObject) {
            try {
                this.syncObject.wait();
            } catch (InterruptedException e) {
            }

        }
    }

    public void CheckForDependencies(IService service) {
       
        this.Dependecies.remove(service.GetName());
        
        if (this.Dependecies.isEmpty()) {
            synchronized (this.syncObject) {
                this.syncObject.notifyAll();
            }
        }
    }
}
