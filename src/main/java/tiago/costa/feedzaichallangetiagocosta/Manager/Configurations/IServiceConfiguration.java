/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Manager.Configurations;

import java.util.List;

/**
 *
 * @author tiagocosta
 */
interface IServiceConfiguration {

    public String GetName();

    public List<String> GetDependencies();
}
