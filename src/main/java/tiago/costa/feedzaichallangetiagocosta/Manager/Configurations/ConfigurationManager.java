/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Manager.Configurations;

import com.google.gson.Gson;
import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.List;

/**
 *
 * @author tiagocosta
 */
public class ConfigurationManager implements IConfigurationManager {

    private final Configuration configuration;

    public ConfigurationManager(String ConfigurationPath, Gson gson) throws FileNotFoundException {
       
        BufferedReader bufferedReader = new BufferedReader(new FileReader(ConfigurationPath));
        this.configuration = gson.fromJson(bufferedReader, Configuration.class);
    }
    
    
    @Override
    public List<ServiceConfiguration> GetServicesConfigurtion() {
        return this.configuration.Services;
    }

}
