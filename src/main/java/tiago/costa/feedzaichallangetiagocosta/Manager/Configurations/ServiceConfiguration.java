/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Manager.Configurations;

import java.util.List;

/**
 *
 * @author tiagocosta
 */
public class ServiceConfiguration implements IServiceConfiguration {
    
    protected String Name;
    protected List<String> Dependecies;

    
    @Override
    public String GetName(){
        return this.Name;
    }
    

    @Override
    public List<String> GetDependencies() {
        return this.Dependecies;
    }
}
