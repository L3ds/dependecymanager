/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Manager;

import java.util.List;
import tiago.costa.feedzaichallangetiagocosta.Services.IService;

/**
 *
 * @author tiagocosta
 */
public interface IDependecyManager {
    
    public void WaitForDependeciesToStart(List<String> dependeciesName);

    public void AddRunningService(IService service);
}
