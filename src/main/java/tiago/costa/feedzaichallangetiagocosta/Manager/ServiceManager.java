/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Manager;

import java.util.List;
import java.util.stream.Collectors;
import tiago.costa.feedzaichallangetiagocosta.Manager.Configurations.ConfigurationManager;
import tiago.costa.feedzaichallangetiagocosta.Services.IServiceFactory;
import tiago.costa.feedzaichallangetiagocosta.Manager.Configurations.ServiceConfiguration;
import tiago.costa.feedzaichallangetiagocosta.Services.IService;

/**
 *a
 * @author tiagocosta
 */
public class ServiceManager implements IServiceManager {
    
    private final ConfigurationManager configurationManager;
    private IDependecyManager dependecyManager;
    private IServiceFactory serviceFactory;
    private List<IService> services;
    
    public ServiceManager(
            ConfigurationManager configurationManager,
            IDependecyManager dependecyManager,
            IServiceFactory serviceFactory){
        
        if(configurationManager ==null) throw new IllegalArgumentException();
        
        this.configurationManager=configurationManager;
        this.serviceFactory = serviceFactory;
        this.dependecyManager = dependecyManager;
    }

    @Override
    public void StartAll() {
       
        List<ServiceConfiguration> configuration = this.configurationManager.GetServicesConfigurtion();
        
        this.services = configuration.parallelStream().map(x -> this.startService(x)).collect(Collectors.toList());
    }
    
    
    
    private IService startService(ServiceConfiguration serviceConfiguration)
    {
        IService service = this.serviceFactory.Create(
                serviceConfiguration.GetName(),
                serviceConfiguration.GetDependencies(),
                this.dependecyManager);
        
        service.Start();
                
        return service;
    }
    
   
}
