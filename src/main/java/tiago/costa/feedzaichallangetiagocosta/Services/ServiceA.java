/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Services;

import java.util.List;
import tiago.costa.feedzaichallangetiagocosta.Manager.IDependecyManager;

/**
 *
 * @author tiagocosta
 */
public class ServiceA extends ServiceAbstract {

    /**
     *
     * @param manager
     * @param dependecies
     */
    public ServiceA(IDependecyManager manager, List<String> dependecies) {
        super(manager, dependecies);
        this.serviceName = this.getClass().getName();
    }

}
