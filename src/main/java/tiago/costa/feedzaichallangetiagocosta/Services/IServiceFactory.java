/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Services;

import java.util.List;
import tiago.costa.feedzaichallangetiagocosta.Manager.IDependecyManager;

/**
 *
 * @author tiagocosta
 */
public interface IServiceFactory {
    
    public IService Create(String className,List<String> dependecies,IDependecyManager manager);
    
}
