/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Services;

import java.lang.reflect.InvocationTargetException;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import tiago.costa.feedzaichallangetiagocosta.Manager.IDependecyManager;

/**
 *
 * @author tiagocosta
 */
public class ServiceFactory implements IServiceFactory {

    public ServiceFactory() {
        
    }
    
    @Override
    public IService Create(String className,List<String> dependecies,IDependecyManager manager) {

        try {
            Object[] params = {manager,dependecies};
            Class<?>[] cParams = new Class<?>[] {IDependecyManager.class,Class.forName("java.util.List")};
            return (IService) Class
                    .forName(className)
                    .getConstructor(cParams)
                    .newInstance(params);

        } catch (ClassNotFoundException
                | NoSuchMethodException
                | SecurityException
                | InstantiationException
                | IllegalAccessException
                | IllegalArgumentException
                | InvocationTargetException ex) {

            Logger
                    .getLogger(ServiceFactory.class.getName())
                    .log(Level.SEVERE, null, ex);
        }

        return null;
    }

   

}
