/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Services;

import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;
import tiago.costa.feedzaichallangetiagocosta.Manager.IDependecyManager;
import tiago.costa.feedzaichallangetiagocosta.Manager.IServiceManager;

/**
 *
 * @author tiagocosta
 */
public abstract class ServiceAbstract implements IService {

    private final IDependecyManager dependecyManager;
    private final List<String> dependencies;
    private final Thread thread;
    
    public ServiceAbstract(IDependecyManager manager,List<String> Dependencies)
    {
        this.dependecyManager = manager;
        this.dependencies = Dependencies;
        this.thread = new Thread(this);
    }
    
    protected String serviceName;
    
    @Override
    public void Start() 
    {
      
        
        this.thread.start();
        
    }

    @Override
    public void End() {
        try {
            //Do missing the checks in here
            this.thread.interrupt();
        } catch (Throwable ex) {
            Logger.getLogger(ServiceAbstract.class.getName()).log(Level.SEVERE, null, ex);
        }
    }

    @Override
    public String GetName() {
        return this.serviceName; 
    }

    @Override
    public void run() {
        
        this.dependecyManager.WaitForDependeciesToStart(this.dependencies);
        System.out.println(this.serviceName+"-Start"); 
        this.dependecyManager.AddRunningService(this);
    }
    
    
    
}
