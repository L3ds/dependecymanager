/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package tiago.costa.feedzaichallangetiagocosta.Services;

/**
 *
 * @author tiagocosta
 */
public interface IService extends Runnable{
    
    void Start();
    
    void End();
    
    String GetName();
}
